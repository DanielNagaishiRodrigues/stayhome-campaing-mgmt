#!/usr/bin/python3


class CampaignMgmtException(Exception):
    def __init__(self, *args):
        super(CampaignMgmtException, self).__init__(*args)


class CampaignServiceException(CampaignMgmtException):
    def __init__(self, status_code, message):
        self.status_code = status_code
        self.message = message
        self.body = {'message': self.message}
        super(CampaignServiceException, self).__init__(message)


class CampaignNotFoundException(CampaignServiceException):
    def __init__(self):
        super(CampaignNotFoundException, self).__init__(404, "Participação não encontrada")


class InvalidTokenException(CampaignServiceException):
    def __init__(self):
        super(InvalidTokenException, self).__init__(401, "Token inválido!")


class BadRequestException(CampaignServiceException):
    def __init__(self):
        super(BadRequestException, self).__init__(400, "Falha ao validar os dados da campanha")


class NotAllowedException(CampaignServiceException):
    def __init__(self):
        super(NotAllowedException, self).__init__(405, "Localização já processada")
