#!/usr/bin/python3
import pickle
from os import environ

import redis

from campaignmgmt.exceptions.http_exceptions import BadRequestException, NotAllowedException
from utils import date_diff
from ..exceptions.http_exceptions import CampaignNotFoundException, CampaignServiceException
from ..model.models import Campaign, DefaultFinishReasonUser
from ..repository import repository


def cached(func):
    def get_cached(*args, **kwargs):
        cache = redis.Redis(host=environ['REDIS_HOST'], port=environ['REDIS_PORT'], db=environ['REDIS_DB'])
        key = 'stayhome:campaing_count'

        redis_available = False
        cached_configuration = None

        try:
            cached_configuration = cache.get(key)
        except redis.exceptions.ConnectionError:
            print("Redis indisponível")
        else:
            redis_available = True

        if cached_configuration is None:
            cached_configuration = func(*args, **kwargs)

            if cached_configuration is not None and redis_available:
                cache.set(key, pickle.dumps(cached_configuration), ex=environ['REDIS_TTL'])
        else:
            cached_configuration = pickle.loads(cached_configuration)
        return cached_configuration

    return get_cached


def save(campaign):
    if campaign['customer_id'] is None:
        raise BadRequestException()

    try:
        old_campaign = repository.find_campaign_by_customer_id(Campaign.table_name, campaign['customer_id'])

        if old_campaign['finished_at'] == 0:
            raise CampaignServiceException(400, "Campanha ainda está valida")

        if campaign['created_at'] <= old_campaign["created_at"]:
            raise CampaignServiceException(400, "Tempo de campanha incorreto")

        campaign_id = old_campaign['campaign_id']
        old_campaign.update(campaign)

        old_campaign['campaign_id'] = campaign_id

        old_campaign['finished_at'] = 0
        old_campaign['finished_reason'] = ''

        if old_campaign['is_ios'] is None:
            old_campaign['is_ios'] = False

        response = repository.update(Campaign.table_name, old_campaign)
    except CampaignNotFoundException as cnf:
        print(f"[??] new campaign for {campaign['customer_id']}")
        print(cnf)

        campaign = Campaign(None, finished_at=0, **campaign)
        campaign.generate_id()
        response = repository.save(Campaign.table_name, campaign)
    except CampaignServiceException as cse:
        raise cse
    except Exception as ex:
        print(f"[!!] Erro {ex}")
        raise CampaignServiceException(500, str(ex))
    return response


def get(query_parameter):
    campaign_id = query_parameter['campaign_id']
    campaign_as_dict = repository.find_by_id(Campaign.table_name, Campaign.id_attribute, campaign_id)
    return Campaign.from_dict(campaign_as_dict).to_dict()


def validate_campaign(campaign):
    try:
        if 'campaign_id' in campaign:
            campaign_as_dict = repository.find_by_id(Campaign.table_name, Campaign.id_attribute,
                                                     campaign['campaign_id'])

            response = Campaign.from_dict(campaign_as_dict).to_dict()
        else:
            if 'customer_id' in campaign:
                campaign_as_dict = repository.find_campaign_by_user(Campaign.table_name, campaign['customer_id'])

                if ('token_id' in campaign.keys() and len(campaign['token_id']) and
                        campaign_as_dict['token_id'] != campaign['token_id']):

                    campaign_update = Campaign.from_dict(campaign_as_dict)
                    campaign_update.token_id = campaign['token_id']

                    response = campaign_update.to_dict()

                    repository.update(Campaign.table_name, response)
                else:
                    response = campaign_as_dict

            else:
                print(f"[!!] validate_campaign - BadRequest - {campaign}")
                raise BadRequestException()

    except CampaignServiceException as cse:
        raise cse
    except Exception as ex:
        print(f"[!!] Erro {ex}")
        raise CampaignServiceException(500, str(ex))
    return response


def finish_campaign(campaign):
    if 'finished_reason' not in campaign.keys() or not campaign['finished_reason']:
        campaign['finished_reason'] = DefaultFinishReasonUser.reason

    item = repository.finish_campaign(Campaign.table_name, campaign)

    return date_diff(item['Attributes']['created_at'], item['Attributes']['finished_at'])


def campaign_duration(query_parameter):
    campaign_id = query_parameter['campaign_id']
    campaign = repository.find_by_id(Campaign.table_name, Campaign.id_attribute, campaign_id)

    return date_diff(campaign['created_at'], float(query_parameter['now']))


def save_user_location(campaign):
    saved_campaign = repository.find_by_id(Campaign.table_name, Campaign.id_attribute, campaign['campaign_id'])

    if saved_campaign['user_lat'] == '0' and saved_campaign['user_long'] == '0':
        item = repository.save_user_location(Campaign.table_name, campaign['campaign_id'], campaign['latitude'],
                                             campaign['longitude'])
    else:
        print(f"[!!] save_user_location - NotAllowedException - {campaign}")
        raise NotAllowedException()

    return item['Attributes']['campaign_id']


@cached
def count_valid_campaigns(query_parameters):
    return repository.count_valid_entrys(Campaign.table_name)
