#!/usr/bin/pythnon3
# coding: utf-8

import uuid
from collections import namedtuple
from datetime import datetime
from functools import singledispatch
from decimal import Decimal

DefaultFinishReason = namedtuple('DefaultFinishReason', 'reason')
DefaultFinishReasonUser = DefaultFinishReason('user')


class Campaign:
    table_name = 'StayHome_CampaignMgmt'
    id_attribute = 'campaign_id'

    def __init__(self, campaign_id, customer_id, token_id, user_lat, user_long,
                 created_at, finished_at, finished_reason=None, is_ios=False):
        self.campaign_id = campaign_id
        self.customer_id = customer_id
        self.token_id = token_id
        self.user_lat = user_lat
        self.user_long = user_long
        self.created_at = created_at
        self.finished_at = finished_at
        self.finished_reason = finished_reason
        self.is_ios = is_ios

    def generate_id(self):
        self.campaign_id = str(uuid.uuid4())

    def to_dict(self):
        return vars(self)

    @staticmethod
    def from_dict(campaign_as_dict: dict):
        return Campaign(**dict([(key, sanitize(value)) for key, value in campaign_as_dict.items()]))


@singledispatch
def sanitize(data):
    return data


@sanitize.register(datetime)
def _(data):
    return data.isoformat()


@sanitize.register(Decimal)
def _(data):
    return int(data) if data.to_integral_value() == data else float(data)
