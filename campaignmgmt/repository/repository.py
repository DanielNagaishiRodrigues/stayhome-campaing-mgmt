#!/usr/bin/python3
# coding: utf-8

from boto3.dynamodb.conditions import Attr
from botocore.exceptions import ClientError

from campaignmgmt.exceptions.http_exceptions import CampaignNotFoundException
from ..repository.dynamodb.dynamo_engine import table


def save(table_name, item):
    try:
        table(table_name).put_item(Item=item.to_dict())
        return item.campaign_id
    except Exception as e:
        print(f"[!!] save - Erro - {e}")
        raise e


def update(table_name, campaign):
    try:
        item = table(table_name).put_item(Item=campaign)
        print(item)
        return campaign['campaign_id']
    except Exception as e:
        print(f"[!!] refresh campaign - update_item - {e}")
        raise e


def finish_campaign(table_name, campaign):
    campaign_id = campaign['campaign_id']
    finished_at = campaign['finished_at']
    finished_reason = campaign['finished_reason']
    try:
        item = table(table_name).update_item(Key={'campaign_id': campaign_id},
                                             AttributeUpdates={
                                                 'finished_at': {"Action": "PUT",
                                                                 "Value": finished_at},
                                                 'finished_reason': {"Action": "PUT",
                                                                     "Value": finished_reason}
                                             }, ReturnValues='ALL_OLD')

        return item
    except Exception as e:
        print(f"[!!] finish_campaign - update_item - {e}")
        raise e


def save_user_location(table_name, campaign_id, latitude, longitude):
    try:
        item = table(table_name).update_item(Key={'campaign_id': campaign_id},
                                             AttributeUpdates={
                                                 'user_lat': {"Action": "PUT",
                                                              "Value": latitude},
                                                 'user_long': {"Action": "PUT",
                                                               "Value": longitude}
                                             }, ReturnValues='ALL_OLD')

        return item
    except Exception as e:
        print(f"[!!] finish_campaign - update_item - {e}")
        raise e


def find_by_id(table_name, param, campaign_id):
    try:
        response = table(table_name).get_item(Key={param: campaign_id})

        if 'Item' not in response.keys():
            print(f"[!!]find_by_id - CampaignNotFoundException - {response}")
            raise CampaignNotFoundException()
    except ClientError as e:
        print(f"[!!]find_by_id - ClientError - {e}")
        raise CampaignNotFoundException()

    return response['Item']


def find_campaign_by_customer_id(table_name, customer_id):
    campaigns = get_all(table_name, Attr('customer_id').eq(customer_id))

    if not len(campaigns):
        print(f"[!!] find_campaign_by_user - CampaignNotFoundException - {campaigns}")
        raise CampaignNotFoundException()

    return campaigns[0]


def find_campaign_by_user(table_name, customer_id):
    campaign = get_all(table_name, Attr('customer_id').eq(customer_id) & Attr('finished_at').eq(0))

    if not len(campaign):
        print(f"[!!] find_campaign_by_user - CampaignNotFoundException - {customer_id}")
        raise CampaignNotFoundException()

    return campaign[0]


def count_valid_entrys(table_name):
    response = table(table_name).scan(FilterExpression=Attr('finished_at').eq(0) &
                                                       Attr('user_lat').ne('0') &
                                                       Attr('user_long').ne('0'), Select='COUNT')
    counter = response['Count']
    while 'LastEvaluatedKey' in response.keys():
        response = table(table_name).scan(FilterExpression=Attr('finished_at').eq(0) &
                                                           Attr('user_lat').ne('0') &
                                                           Attr('user_long').ne('0'),
                                          ExclusiveStartKey=response['LastEvaluatedKey'], Select='COUNT')
        counter += response['Count']
    return counter


def get_all(table_name, filter_expression):
    response = table(table_name).scan(FilterExpression=filter_expression, IndexName='customer_id-index')
    data = response['Items']
    while 'LastEvaluatedKey' in response.keys():
        response = table(table_name).scan(FilterExpression=filter_expression,
                                          ExclusiveStartKey=response['LastEvaluatedKey'],
                                          IndexName='customer_id-index')
        data.extend(response['Items'])
    return data
