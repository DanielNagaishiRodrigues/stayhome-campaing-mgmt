from itertools import groupby

import boto3

dynamodb = boto3.resource('dynamodb',
                          aws_access_key_id='AKIATQ6SX6CL3PR6KSX5',
                          aws_secret_access_key='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',
                          region_name='us-east-1')


def get_all(table):
    response = table.scan()
    # response = table.scan(FilterExpression=Attr('customer_id').eq('8ba38dc7-1167-4a45-bd44-af1d81528010'))
    data = response['Items']
    while 'LastEvaluatedKey' in response.keys():
        response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
        # response = table.scan(FilterExpression=Attr('customer_id').eq('8ba38dc7-1167-4a45-bd44-af1d81528010'),
        #                       ExclusiveStartKey=response['LastEvaluatedKey'])
        data.extend(response['Items'])
    return data


table = dynamodb.Table('StayHome_CampaignMgmt')
all_campaigns = get_all(table)
print('Todas campanhas: ' + str(len(all_campaigns)))
all_campaigns.sort(key=lambda customer: customer['customer_id'])

campaigns_by_user = []
duplicated_campaigns = []
duplicated_campaigns_by_token = []
duplicated_campaigns_to_clean = []
garbage = []

i = 0
while i < len(all_campaigns):
    campaign = all_campaigns[i]
    remove = False

    if campaign['user_long'] == '' or campaign['user_long'] == '0.0':
        remove = True

    if campaign['user_lat'] == '' or campaign['user_lat'] == '0.0':
        remove = True

    if remove:
        garbage.append(all_campaigns.pop(i))
    else:
        i += 1

for k, v in groupby(all_campaigns, key=lambda x: x['customer_id']):
    campaigns_by_user.append(list(v))

for item in campaigns_by_user:

    if len(item) > 1:
        duplicated_campaigns.append(item)

for item in duplicated_campaigns:
    item.sort(key=lambda x: x['token_id'])

    for k, v in groupby(item, key=lambda x: x['token_id']):
        duplicated_campaigns_by_token.append(list(v))

for item in duplicated_campaigns_by_token:

    i = 0
    while i < len(item):
        campaign = item[i]
        remove = False

        if campaign['finished_at'] != 0:
            remove = True

        if remove:
            del item[i]
        else:
            i += 1

    duplicated_campaigns_to_clean.append(item)

for item in duplicated_campaigns_to_clean:
    item.sort(key=lambda x: x['created_at'])

    if len(item) > 1:
        for index, campaign in enumerate(item):
            if index > 0:
                garbage.append(campaign)

qtd_limpeza = len(garbage)
print('Qtde registros a limpar: ' + str(qtd_limpeza))

for index, item in enumerate(garbage):
    # table.delete_item(Key={'campaign_id': item['campaign_id']})

    print('Limpo: ' + str(index + 1) + '/' + str(qtd_limpeza))
