#!/usr/bin/python3
# coding: utf-8

import os
from collections.abc import MutableMapping
import yaml


def load_config():
    with open('application.yml', 'r', encoding='utf-8') as config_file:
        configuration = yaml.full_load(config_file)
    load_map_configurations(configuration)


def load_map_configurations(configurations, prefix_key=None):
    for config_key, config_value in configurations.items():
        new_key = config_key if prefix_key is None else prefix_key + '.' + config_key
        if isinstance(config_value, MutableMapping):
            load_map_configurations(config_value, prefix_key=new_key)
        else:
            store(new_key, config_value)


def store(key, value):
    os.environ[key.upper()] = str(value)


load_config()