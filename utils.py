#!/usr/bin/python3
# coding: utf-8
import datetime
import json
import os
import decimal


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o) if o % 1 > 0 else int(o)
        return super(DecimalEncoder, self).default(o)


def validate_token(event_headers):
    return not (event_headers is None or
                'x-api-token' not in event_headers.keys() or
                event_headers['x-api-token'] != os.getenv('TOKEN_API'))


def build_response_body(response_body):
    return {"message": response_body}


def jsonify(body):
    return json.dumps(body, cls=DecimalEncoder)


def response_ok(payload, http_status):
    return response(http_status, payload)


def response_nok(status_code, message):
    return response(status_code, {"message": message})


def response(status_code, body):
    return {"statusCode": status_code, "body": jsonify(body), "isBase64Encoded": False}


def date_diff(date_init, date_now):
    init_date = datetime.datetime.fromtimestamp(int(str(date_init)[0:10]))
    end_date = datetime.datetime.fromtimestamp(int(str(date_now)[0:10]))
    delta = end_date - init_date
    days = divmod(delta.total_seconds(), 86400)
    hours = divmod(days[1], 3600)  # Use remainder of days to calc hours

    return {'days': int(days[0]), 'hour': int(hours[0])}
