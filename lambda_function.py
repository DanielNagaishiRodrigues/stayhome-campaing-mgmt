import json

from campaignmgmt.exceptions.http_exceptions import CampaignServiceException
from campaignmgmt.service import campaign_service
from utils import validate_token, response_nok, response_ok

routes = {
    '/stayhome/mgmt/get_qtde_campaigns': {'GET': {'func': campaign_service.count_valid_campaigns, 'status': 200}},
    '/stayhome/mgmt/get_campaign': {'GET': {'func': campaign_service.get, 'status': 200}},
    '/stayhome/mgmt/campaign_duration': {'GET': {'func': campaign_service.campaign_duration, 'status': 200}},
    '/stayhome/mgmt/validate_campaign': {'POST': {'func': campaign_service.validate_campaign, 'status': 200}},
    '/stayhome/mgmt/create_campaign': {'PUT': {'func': campaign_service.save, 'status': 201}},
    '/stayhome/mgmt/finish_campaign': {'POST': {'func': campaign_service.finish_campaign, 'status': 204}},
    '/stayhome/mgmt/save_user_location': {'POST': {'func': campaign_service.save_user_location, 'status': 201}},
}


def lambda_handler(event, context):
    print(json.dumps(event))

    if not validate_token(event["headers"]):
        return response_nok(401, "Não autorizado")

    try:
        body = ''
        if event['body'] is not None:
            body = json.loads(event['body'])

        query_parameter = event['queryStringParameters']

        resource = event['resource']
        method = event['httpMethod']

        if method == 'GET':
            return response_ok(routes[resource][method]['func'](query_parameter), routes[resource][method]['status'])
        else:
            return response_ok(routes[resource][method]['func'](body), routes[resource][method]['status'])
    except CampaignServiceException as cse:
        return response_nok(cse.status_code, cse.message)
    except (TypeError, KeyError) as e:
        print(e)
        return response_nok(400, "Bad Request")
    except Exception as e:
        print(e)
        return response_nok(500, "Internal Server Error")


# with open('scripts/json_test.json') as event_json:
#     data = json.load(event_json)
#
# lambda_handler(event=data, context=None)
